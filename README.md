# Roboccino

## Project description

This project was a part of my BSc thesis, whose objective was to use the collaborative robot to perform experiments aimed at utilizing different optimization methods to search the system's parameter space for a resulting product with the best indices. The case-study was powdered cappuccino whose foam after preparation was evaluated with computer vision tools. The usage of computer vision driven feedback in closed loop control for hot drink foam quality improvement was also investigated.

## Repository contents 

This repository contains the *roboccino* package, being the main control software for the whole setup. The detailed descriptions of all the nodes and their functions, the parameter files and additional image analysis scripts can be found in my [thesis](https://emilia-szymanska.gitlab.io/cv/docs/7_sem/BSc_thesis.pdf). 

## Installation instructions

Make sure to follow the setup instructions mentioned in the [corresponding repository](https://gitlab.com/roboccino/setup-bash-scripts). The [optimization scripts](https://gitlab.com/roboccino/optimization-scripts) allowed for exploring the Bayesian, grid Bayesian and TPE optimizers. To run *roboccino* package, [roboccino_interfaces](https://gitlab.com/roboccino/roboccino_interfaces) package also has to be in the ROS2 *src* directory and [these programs](https://gitlab.com/roboccino/roboccino-supplementary-programs) flashed to Arduino boards.