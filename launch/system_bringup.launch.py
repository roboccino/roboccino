from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node
import os

def generate_launch_description():
    ld = LaunchDescription()
    config = os.path.join(
        get_package_share_directory('roboccino'),
        'config',
        'parameters.yaml'
        )
        
    robot_node = Node(
            package = "roboccino",
            executable = "robot",
            name = "robot",
            output = "screen",
            emulate_tty = True,
            parameters=[config]
            )
    
    water_dispenser_node = Node(
            package = "roboccino",
            executable = "water_dispenser",
            name = "water_dispenser",
            output = "screen",
            emulate_tty = True,
            parameters=[config]
            )
    
    powder_dispenser_node = Node(
            package = "roboccino",
            executable = "powder_dispenser",
            name = "powder_dispenser",
            output = "screen",
            emulate_tty = True,
            parameters=[config]
            )
    
    stirrer_node = Node(
            package = "roboccino",
            executable = "stirrer",
            name = "stirrer",
            output = "screen",
            emulate_tty = True,
            parameters=[config]
            )
    
    motor_shield_commander_node = Node(
            package = "roboccino",
            executable = "motor_shield_commander",
            name = "motor_shield_commander",
            output = "screen",
            emulate_tty = True,
            parameters=[config]
            )
    
    servo_commander_node = Node(
            package = "roboccino",
            executable = "servo_commander",
            name = "servo_commander",
            output = "screen",
            emulate_tty = True,
            parameters=[config]
            )
    
    bubble_camera_node = Node(
            package = "roboccino",
            executable = "camera",
            name = "bubble_camera",
            output = "screen",
            emulate_tty = True,
            remappings=[
                ('camera_command', 'bubble_camera_command'),
                ('camera_status', 'bubble_camera_status'),
                ('image', 'bubble_image')],
            parameters=[config]
            )
    
    side_camera_node = Node(
            package = "roboccino",
            executable = "camera",
            name = "side_camera",
            output = "screen",
            emulate_tty = True,
            remappings=[
                ('camera_command', 'side_camera_command'),
                ('camera_status', 'side_camera_status'),
                ('image', 'side_image')],
            parameters=[config]
            )
    
    controller_node = Node(
            package = "roboccino",
            executable = "controller",
            name = "controller",
            output = "screen",
            emulate_tty = True
            )
    
    image_analyzer_node = Node(
            package = "roboccino",
            executable = "image_analyzer",
            name = "image_analyzer",
            output = "screen",
            emulate_tty = True,
            parameters=[config]
            )
    
    ramp_node = Node(
            package = "roboccino",
            executable = "ramp",
            name = "ramp",
            output = "screen",
            emulate_tty = True,
            parameters=[config]
            )
    
    ld.add_action(water_dispenser_node)
    ld.add_action(powder_dispenser_node)
    ld.add_action(bubble_camera_node)
    ld.add_action(side_camera_node)
    ld.add_action(image_analyzer_node)
    ld.add_action(controller_node)
    ld.add_action(stirrer_node)
    ld.add_action(motor_shield_commander_node)
    ld.add_action(servo_commander_node)
    ld.add_action(ramp_node)
    ld.add_action(robot_node)
    return ld
