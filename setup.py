from setuptools import setup
import os
from glob import glob

package_name = 'roboccino'
supplementary_package = 'supplementary_elements'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name, supplementary_package],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (os.path.join('share', package_name, 'launch'), glob('launch/*.launch.py')),
        (os.path.join('share', package_name, 'config'), glob('config/*.yaml')),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='emilia-szymanska',
    maintainer_email='emilia.szymanska.eka@gmail.com',
    description='ROS2 package for controlling the system with a UR5 robot, camera and additional equipment aimed at preparing cappuccino.',
    license='MIT License',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'controller = roboccino.main_controller:main',
            'water_dispenser = roboccino.water_dispenser:main',
            'powder_dispenser = roboccino.powder_dispenser:main',
            'camera = roboccino.camera:main',
            'image_analyzer = roboccino.image_analyzer:main',
            'robot = roboccino.robot:main',
            'ramp = roboccino.ramp:main',
            'stirrer = roboccino.stirrer:main',
            'motor_shield_commander = roboccino.motor_shield_commander:main',
            'servo_commander = roboccino.servo_commander:main',
        ],
    },
)
