from enum import Enum

class StirrerState(Enum):
    UNINITIALIZED = -1
    INITIALIZE = 0
    STIR = 1
