import cv2
import numpy as np
from math import sqrt

def crop_image(img, x, y, width, height):
    cropped_img = img[y:y+height, x:x+width]
    return cropped_img

def create_circular_mask(img, r, is_rgb):
    hh, ww = img.shape[:2]
    xc = ww // 2
    yc = hh // 2
    if(is_rgb):
        mask = np.zeros_like(img)
        mask = cv2.circle(mask, (xc, yc), r, (255, 255, 255), -1)
    else:
        mask = np.zeros(img.shape[:2], dtype='uint8')
        mask = cv2.circle(mask, (xc, yc), r, 255, -1)
    return mask


def get_distance(x, y, X, Y):
    return sqrt(abs(x-X)**2 + abs(y-Y)**2)


def clustering(image, k):
    # reshape the image to a 2D array of pixels and 3 color values (RGB)
    pixel_values = image.reshape((-1, 3))
    pixel_values = np.float32(pixel_values)

    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 0.2)
    ret, labels, (centers) = cv2.kmeans(pixel_values, k, None, criteria, 10, cv2.KMEANS_RANDOM_CENTERS)

    # convert back to 8 bit values
    centers = np.uint8(centers)
    # flatten the labels array
    labels = labels.flatten()
    # convert all pixels to the color of the centroids
    segmented_image = centers[labels.flatten()]
    # reshape back to the original image dimension
    segmented_image = segmented_image.reshape(image.shape)
    return segmented_image


def get_grayscale_histogram(img, mask = None):
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    hist = cv2.calcHist([img_gray], [0], mask, [256], [0, 256])
    x_values = []
    y_values = []
    for i in range(len(hist)):
        if(hist[i] != 0):
            x_values.append(str(i))
            y_values.append(hist[i][0])
    return hist, x_values, y_values
