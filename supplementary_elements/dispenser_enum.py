from enum import Enum

class DispenserState(Enum):
    UNINITIALIZED = -1
    INITIALIZE = 0
    POUR = 1
