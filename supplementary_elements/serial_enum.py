from enum import Enum

class SerialState(Enum):
    UNINITIALIZED = -1
    INITIALIZE = 0
    CLOSE = 1
