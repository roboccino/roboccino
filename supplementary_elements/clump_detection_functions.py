import sys
import cv2
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt
from supplementary_elements.extra_functions import crop_image, create_circular_mask

def calculate_clump_matrix(img, window_x, window_y, threshold):
    (rows, columns) = img.shape
    
    x_iterations = rows // window_x
    if(rows % window_x != 0):
        x_iterations += 1

    y_iterations = columns // window_y
    if(columns % window_y != 0):
        y_iterations += 1
    
    result = np.zeros((x_iterations, y_iterations))
    result_bin = np.zeros((x_iterations, y_iterations))
    gray_value = stats.mode(img, axis=None).mode[0]

    max_i = 0
    max_j = 0
    max_val = 0

    for i in range(x_iterations):
        for j in range(y_iterations):
            window = img[(i*window_x):((i+1)*window_x), j*window_y:(j+1)*window_y]
            value = analyze_window(window, gray_value)
            result[i, j] = value
            if(value >= threshold):
                result_bin[i, j] = value
            if(value >= max_val):
                max_i = i
                max_j = j
                max_val = value
            
    result_bin[max_i, max_j] = max_val 
    np.set_printoptions(suppress=True)
    return result, result_bin



def analyze_window(window, gray_value):
    difference_sum = 0
    for el in np.nditer(window):
        difference_sum += np.absolute(el - gray_value)
    return difference_sum


def detect_foam_clumps(img, r, window_x, window_y, threshold):
    clumps = False
    laplacian = cv2.Laplacian(img, cv2.CV_64F)
    mask_lpc = create_circular_mask(laplacian, r, is_rgb = True)
    laplacian = cv2.bitwise_and(laplacian, mask_lpc)
    
    result_lap, matrix_lap = calculate_clump_matrix(laplacian, window_x, window_y, threshold)
    
    if(matrix_lap.max() >= threshold):
        clumps = True
    
    fig = plt.figure()
    ax1 = fig.add_subplot(1, 2, 1)
    im1 = ax1.imshow(result_lap)
    fig.colorbar(im1)
    ax1.set_title('Laplacian transform')
 
    ax2 = fig.add_subplot(1, 2, 2)
    im2 = ax2.imshow(matrix_lap)
    ax2.set_title('Output matrix')
    fig.colorbar(im2)

    return fig, matrix_lap, clumps


def detect_liquid_clumps(img, window_x, window_y, threshold):
    clumps = False
    laplacian = cv2.Laplacian(img, cv2.CV_64F)
    
    result_lap, matrix_lap = calculate_clump_matrix(laplacian, window_x, window_y, threshold)
    
    if(matrix_lap.max() >= threshold):
        clumps = True

    fig = plt.figure()
    ax1 = fig.add_subplot(1, 2, 1)
    im1 = ax1.imshow(result_lap)
    fig.colorbar(im1)
    ax1.set_title('Laplacian transform')
 
    ax2 = fig.add_subplot(1, 2, 2)
    im2 = ax2.imshow(matrix_lap)
    ax2.set_title('Output matrix')
    fig.colorbar(im2)

    return fig, matrix_lap, clumps
