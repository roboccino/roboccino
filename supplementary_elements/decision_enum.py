from enum import Enum

class Decision(Enum):
    WAITING = -1
    NOT_READY = 0
    READY = 1
    FOAM_CLUMPS = 2
    LIQUID_CLUMPS = 3
    BIG_BUBBLES = 4
