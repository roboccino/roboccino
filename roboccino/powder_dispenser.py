from supplementary_elements.dispenser_enum import DispenserState
from roboccino_interfaces.msg import PowderDispenserParams
from supplementary_elements.error_enum import ErrorHandling

from rclpy.exceptions import ParameterNotDeclaredException
from rcl_interfaces.msg import ParameterType
from std_msgs.msg import Int8
from rclpy.node import Node
import rclpy

import signal
import serial
import struct
import sys


class PowderDispenser(Node):

    def __init__(self):
        super().__init__('powder_dispenser')
        
        self.declare_parameter('rpm', 10)
        self.declare_parameter('steps', 34)

        self.subscription_command = self.create_subscription(Int8, 
                            'powder_dispenser_command', self.command_callback, 10)
        self.publisher_status = self.create_publisher(Int8, 'powder_dispenser_status', 10)
        self.publisher_serial_command = self.create_publisher(PowderDispenserParams, 
                            'dispenser_motor_command', 10)
        self.subscription_error = self.create_subscription(Int8, 'error_signal',
                                                self.error_callback, 10)
        self.publisher_error = self.create_publisher(Int8, 'error_signal', 10)


        self.rpm = 0
        self.steps = 0
        signal.signal(signal.SIGINT, self.signal_handler) 


    def init_stepper(self):
        self.rpm = self.get_parameter('rpm').get_parameter_value().integer_value
        self.steps = self.get_parameter('steps').get_parameter_value().integer_value


    def command_callback(self, msg):
        cmd = DispenserState(msg.data)
        if(cmd == DispenserState.INITIALIZE):
            self.init_stepper()
            self.update_status(cmd)
        
        elif(cmd == DispenserState.POUR):
            self.send_motor_command()


    def update_status(self, msg):
        feedback = Int8()
        feedback.data = msg.value
        self.publisher_status.publish(feedback)
        self.get_logger().info('%s' % msg)
    

    def send_motor_command(self):
        cmd = PowderDispenserParams()
        cmd.rpm   = self.rpm
        cmd.steps = self.steps
        self.publisher_serial_command.publish(cmd)

    
    def send_error_signal(self, msg):
        to_send = Int8()
        to_send.data = msg.value
        self.publisher_error.publish(to_send)
        self.get_logger().info('%s' % msg)
        self.kill_node()


    def error_callback(self, msg):
        self.kill_node()


    def signal_handler(self, signal, frame): 
        self.kill_node()

    def kill_node(self):
        self.rpm = 0
        self.steps = 0
        self.send_motor_command()
        self.destroy_node()
        sys.exit(0)



def main(args=None):
    rclpy.init(args=args)

    powder_dispenser = PowderDispenser()
    rclpy.spin(powder_dispenser)
    
    powder_dispenser.kill_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
