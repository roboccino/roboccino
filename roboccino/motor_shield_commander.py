from supplementary_elements.stirrer_enum import StirrerState
from supplementary_elements.serial_enum import SerialState
from supplementary_elements.dispenser_enum import DispenserState
from roboccino_interfaces.msg import StirringParams, PowderDispenserParams
from supplementary_elements.error_enum import ErrorHandling

from rclpy.exceptions import ParameterNotDeclaredException
from rcl_interfaces.msg import ParameterType
from std_msgs.msg import Int8
from rclpy.node import Node
import rclpy

import signal
import serial
import struct
import sys

POWDER_DISPENSER_BYTE = 0
STIRRER_BYTE = 1
MAX_STIRRER_SPEED = 255

class MotorShieldCommander(Node):

    def __init__(self):
        super().__init__('motor_shield_commander')
        
        self.declare_parameter('device', '/dev/ttyACM1')
        self.declare_parameter('baud_rate', 9600)
        
        self.subscription_serial_command = self.create_subscription(Int8, 
                            'serial_motor_command', self.serial_command_callback, 10)
        self.publisher_serial_status = self.create_publisher(Int8, 'serial_motor_status', 10)

        self.subscription_stirrer_command = self.create_subscription(StirringParams, 
                            'stirrer_motor_command', self.stirrer_motor_command_callback, 10)
        self.publisher_stirrer_status = self.create_publisher(Int8, 'stirrer_status', 10)
        
        self.subscription_dispenser_command = self.create_subscription(PowderDispenserParams, 
                            'dispenser_motor_command', self.dispenser_motor_command_callback, 10)
        self.publisher_dispenser_status = self.create_publisher(Int8, 'powder_dispenser_status', 10)
        
        self.subscription_error = self.create_subscription(Int8, 'error_signal',
                                                self.error_callback, 10)
        self.publisher_error = self.create_publisher(Int8, 'error_signal', 10)
        
        self.serial = None
        signal.signal(signal.SIGINT, self.signal_handler) 


    def init_serial(self):
        dev = self.get_parameter('device').get_parameter_value().string_value
        br = self.get_parameter('baud_rate').get_parameter_value().integer_value
        self.get_logger().info('Serial device: "%s"' % dev)
        self.get_logger().info('Baud rate: "%d"' % br)
        self.serial = serial.Serial(dev, br)


    def serial_command_callback(self, msg):
        cmd = SerialState(msg.data)
        if(cmd == SerialState.INITIALIZE):
            self.init_serial()
            self.update_serial_status(cmd)
        elif(cmd == SerialState.CLOSE):
            self.serial.close()
            self.update_serial_status(cmd)


    def stirrer_motor_command_callback(self, msg):
        time = msg.time
        speed = int(msg.speed / 100 * MAX_STIRRER_SPEED)
        time_rx, speed_rx = self.send_and_receive_serial('stirrer', time, speed)
        if(speed_rx == speed and time_rx == time):
            self.update_stirrer_status(StirrerState.STIR)


    def dispenser_motor_command_callback(self, msg):
        rpm = msg.rpm
        steps = msg.steps
        rpm_rx, steps_rx = self.send_and_receive_serial('powder_dispenser', rpm, steps)
        if(rpm_rx == rpm and steps_rx == steps):
            self.update_powder_dispenser_status(DispenserState.POUR)

    
    def send_and_receive_serial(self, peripheral, param1, param2):
        if(peripheral == 'powder_dispenser'):
            self.serial.write(struct.pack('>BBB', POWDER_DISPENSER_BYTE, param1, param2))
        elif(peripheral == 'stirrer'):
            self.serial.write(struct.pack('>BBB', STIRRER_BYTE, param1, param2))
        
        param1_feedback = self.serial.readline()
        param1_response = int(param1_feedback.decode())
        param2_feedback = self.serial.readline()
        param2_response = int(param2_feedback.decode())

        return param1_response, param2_response


    def send_error_signal(self, msg):
        to_send = Int8()
        to_send.data = msg.value
        self.publisher_error.publish(to_send)
        self.get_logger().info('%s' % msg)
        self.kill_node()


    def update_powder_dispenser_status(self, msg):
        feedback = Int8()
        feedback.data = msg.value
        self.publisher_dispenser_status.publish(feedback)
        self.get_logger().info('%s' % msg)

    
    def update_stirrer_status(self, msg):
        feedback = Int8()
        feedback.data = msg.value
        self.publisher_stirrer_status.publish(feedback)
        self.get_logger().info('%s' % msg)

    
    def update_serial_status(self, msg):
        feedback = Int8()
        feedback.data = msg.value
        self.publisher_serial_status.publish(feedback)
        self.get_logger().info('%s' % msg)


    def error_callback(self, msg):
        self.kill_node()

    def kill_node(self):
        if(self.serial is not None):
            self.serial.close()
        self.destroy_node()
        sys.exit(0)

    def signal_handler(self, signal, frame): 
        self.kill_node()


def main(args=None):
    rclpy.init(args=args)

    motor_shield_commander = MotorShieldCommander()
    rclpy.spin(motor_shield_commander)
    
    motor_shield_commander.kill_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
