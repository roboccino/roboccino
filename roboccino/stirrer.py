from supplementary_elements.stirrer_enum import StirrerState
from supplementary_elements.error_enum import ErrorHandling
from roboccino_interfaces.msg import StirringParams


from rclpy.exceptions import ParameterNotDeclaredException
from rcl_interfaces.msg import ParameterType
from std_msgs.msg import Int8
from rclpy.node import Node
import rclpy

import signal
import serial
import struct
import sys


class Stirrer(Node):

    def __init__(self):
        super().__init__('stirrer')
        
        self.declare_parameter('time', 10)
        self.declare_parameter('speed', 10)

        self.subscription_command = self.create_subscription(Int8, 
                            'stirrer_command', self.command_callback, 10)
        self.publisher_status = self.create_publisher(Int8, 'stirrer_status', 10)
        self.publisher_serial_command = self.create_publisher(StirringParams, 
                            'stirrer_motor_command', 10)
        
        self.subscription_parameters = self.create_subscription(StirringParams, 
                            'stirring_params', self.params_callback, 10)
        
        self.subscription_error = self.create_subscription(Int8, 'error_signal',
                                                self.error_callback, 10)
        self.publisher_error = self.create_publisher(Int8, 'error_signal', 10)

        self.speed = 0
        self.time = 0
        signal.signal(signal.SIGINT, self.signal_handler) 


    def init_motor(self):
        self.speed = self.get_parameter('speed').get_parameter_value().integer_value
        self.time = self.get_parameter('time').get_parameter_value().integer_value


    def command_callback(self, msg):
        cmd = StirrerState(msg.data)
        if(cmd == StirrerState.INITIALIZE):
            self.init_motor()
            self.update_status(cmd)
        
        if(cmd == StirrerState.STIR):
            self.send_motor_command()


    def params_callback(self, msg):
        self.speed = msg.speed
        self.time  = msg.time


    def update_status(self, msg):
        feedback = Int8()
        feedback.data = msg.value
        self.publisher_status.publish(feedback)
        self.get_logger().info('%s' % msg)
    

    def send_motor_command(self):
        cmd = StirringParams()
        cmd.time  = self.time
        cmd.speed = self.speed
        self.publisher_serial_command.publish(cmd)


    def signal_handler(self, signal, frame): 
        self.kill_node()

    def send_error_signal(self, msg):
        to_send = Int8()
        to_send.data = msg.value
        self.publisher_error.publish(to_send)
        self.get_logger().info('%s' % msg)
        self.kill_node()


    def error_callback(self, msg):
        self.kill_node()

    def kill_node(self):
        self.destroy_node()
        sys.exit(0)



def main(args=None):
    rclpy.init(args=args)

    stirrer = Stirrer()
    rclpy.spin(stirrer)
    
    stirrer.kill_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
